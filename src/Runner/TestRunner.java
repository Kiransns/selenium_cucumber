package Runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

package org.cuccumber.steps;


@RunWith(Cucumber.class)
@CucumberOptions(features ="src/MyAppalication.feature",glue= {"src/Runner/StepDefinition.java"}, plugin= {"pretty", "html:target/fblogin-report"})

public class TestRunner {


}

